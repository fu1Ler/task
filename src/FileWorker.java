import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FileWorker {
    private File file = new File("Task.txt");
    private char[] text = new char[200];   //text form file.
    private String resultNumbers = ""; //returns only numbers
    private List<Integer> sortedResult = new ArrayList(); // descending sorted numbers.

    public FileWorker(File file) {
        this.file = file;
    }

    private FileWorker() {

    }

    public String read() {
        // Создание файла
        try {
            // Создание объекта FileReader
            FileReader fr = new FileReader(file);
            fr.read(text);   // Чтение содержимого в массив
            fr.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return String.valueOf(text);
    }

    public void write(String string) {
        try {
            // Создание объекта FileWriter
            FileWriter writer = null;
            writer = new FileWriter(file);
            // Запись содержимого в файл
            writer.write(string);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public void onlyNumbers() {
        for (int i = 0; i < text.length; i++) {
            if (text[i] >= 48 && text[i] <= 57) {
                resultNumbers += text[i];
            }
        }
    }

    public void descendingSort() {
        onlyNumbers();
        char[] arrayOfNumbers = resultNumbers.toCharArray();
        for (int i = 0; i < arrayOfNumbers.length; i++) {
            String element = String.valueOf(arrayOfNumbers[i]);
            if (!sortedResult.contains(Integer.valueOf(element))) {
                sortedResult.add(Integer.valueOf(element));
            }
        }
        sortedResult.sort(Comparator.reverseOrder());
    }

    public File getFile() {
        return file;
    }

    public char[] getText() {
        return text;
    }

    public String getResultNumbers() {
        return resultNumbers;
    }

    public List<Integer> getSortedResult() {
        return sortedResult;
    }
}
