import java.io.File;

public class Main {
    public static void main(String[] args) {
        File file = new File("Task.txt");
        FileWorker fileWorker = new FileWorker(file);

        System.out.println("----------Text from " + file.getName() + ":----------");
        System.out.println(fileWorker.read() + "\n");

        System.out.println("----------Numbers form " + file.getName() + ":----------");
        fileWorker.onlyNumbers();
        System.out.println(fileWorker.getResultNumbers() + "\n");

        System.out.println("----------Sorted numbers form " + file.getName() + ":----------");
        fileWorker.descendingSort();
        System.out.println(fileWorker.getSortedResult() + "\n");

    }
}
